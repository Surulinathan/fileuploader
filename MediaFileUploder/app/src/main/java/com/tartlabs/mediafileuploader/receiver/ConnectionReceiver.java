package com.tartlabs.mediafileuploader.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.tartlabs.mediafileuploader.backgroudService.FileUploaderBackgroundService;
import com.tartlabs.mediafileuploader.model.UploadFiles;
import com.tartlabs.mediafileuploader.model.UploadItem;
import com.tartlabs.mediafileuploader.service.FileUploadService;
import com.tartlabs.mediafileuploader.util.SharedPrefsUtils;

import static com.tartlabs.mediafileuploader.constant.Constants.ACCOUNT_PREFS;
import static com.tartlabs.mediafileuploader.constant.Constants.BASE_URL;
import static com.tartlabs.mediafileuploader.constant.Constants.SYNC;
import static com.tartlabs.mediafileuploader.constant.Constants.UPLOAD_FILES;
import static com.tartlabs.mediafileuploader.util.AppUtils.isInternetAvailable;
import static com.tartlabs.mediafileuploader.util.AppUtils.isNetworkAvailable;
import static com.tartlabs.mediafileuploader.util.ConversionUtils.getJsonFromString;

public class ConnectionReceiver extends BroadcastReceiver {

    private Context context;

    public ConnectionReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        showToast("Network Available " + isNetworkAvailable(context));
        showToast("Internet Available " + isInternetAvailable());
        if (isNetworkAvailable(context) && isInternetAvailable()) {
            boolean syc = SharedPrefsUtils.getBoolean(ACCOUNT_PREFS, SYNC, false);
            if (syc) {
                String uploadFilesStr = null;
                UploadFiles uploadFiles = null;

                // get upload media files string to shared preference
                uploadFilesStr = SharedPrefsUtils.getString(ACCOUNT_PREFS, UPLOAD_FILES);

                if (uploadFilesStr != null && !uploadFilesStr.isEmpty()) {

                    // convert  string  to upload item object file
                    uploadFiles = getJsonFromString(uploadFilesStr, UploadFiles.class);

                    for (UploadItem uploadItem : uploadFiles.getUploadItemList()) {
                        if (uploadItem.getMediaList() != null && uploadItem.getMediaList().size() > 0) {   // get notification id
                            int notificationId = uploadItem.getNotificationId();
                            // start service
                            Intent intentService = new Intent(context, FileUploaderBackgroundService.class);
                            intentService.putExtra("notificationId", notificationId);
                            context.startService(intentService);
                            FileUploadService.serviceCall(BASE_URL, "image[]", uploadItem.getMediaList().get(0).getUri(), uploadItem, notificationId);
                        }
                    }
                }
            }
        }
    }

    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}