package com.tartlabs.mediafileuploader;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tartlabs.mediafileuploader.backgroudService.FileUploaderBackgroundService;
import com.tartlabs.mediafileuploader.model.Media;
import com.tartlabs.mediafileuploader.model.UploadFiles;
import com.tartlabs.mediafileuploader.model.UploadItem;
import com.tartlabs.mediafileuploader.service.FileUploadService;
import com.tartlabs.mediafileuploader.util.SharedPrefsUtils;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.tartlabs.mediafileuploader.constant.Constants.ACCOUNT_PREFS;
import static com.tartlabs.mediafileuploader.constant.Constants.BASE_URL;
import static com.tartlabs.mediafileuploader.constant.Constants.UPLOAD_FILES;
import static com.tartlabs.mediafileuploader.util.AppUtils.getPath;
import static com.tartlabs.mediafileuploader.util.AppUtils.isInternetAvailable;
import static com.tartlabs.mediafileuploader.util.AppUtils.isNetworkAvailable;
import static com.tartlabs.mediafileuploader.util.ConversionUtils.getJsonFromString;
import static com.tartlabs.mediafileuploader.util.ConversionUtils.getStringFromJson;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final int RC_PERM_STORAGE = 900;
    private static final int SELECT_MULTIPLE_MEDIA_FILE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPermissions();
            }
        });
    }

    private void galleryMultipleMediaChoose() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Multiple Picture"), SELECT_MULTIPLE_MEDIA_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<Media> mediaList = new ArrayList<>();
        UploadItem uploadItem = new UploadItem();
        int uploadFileCount = 0;
        if (isNetworkAvailable(this) && isInternetAvailable()) {
            if (requestCode == SELECT_MULTIPLE_MEDIA_FILE && resultCode == RESULT_OK) {
                // Get the Image from data
                if (data.getData() != null) {
                    Uri selectedMediaUri = data.getData();
                    Media media = new Media();
                    media.setUri(getPath(this, selectedMediaUri));
                    if (selectedMediaUri.toString().contains("image")) {
                        //handle image
                        media.setType("image");
                    } else if (selectedMediaUri.toString().contains("video")) {
                        //handle video
                        media.setType("video");
                    }


                    mediaList.add(media);
                    int notificationId = (int) System.currentTimeMillis();
                    // set media list into object
                    uploadItem.setNotificationId(notificationId);
                    uploadItem.setMediaList(mediaList);


                    // service call
                    serviceCall("image[]", mediaList.get(0).getUri(), uploadItem, notificationId);
                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri selectedMediaUri = item.getUri();
                            Media media = new Media();
                            media.setUri(getPath(this, selectedMediaUri));
                            if (selectedMediaUri.toString().contains("image")) {
                                //handle image
                                media.setType("image");
                            } else if (selectedMediaUri.toString().contains("video")) {
                                //handle video
                                media.setType("video");
                            }
                            mediaList.add(media);
                        }
                        int notificationId = (int) System.currentTimeMillis();
                        // set media list into object
                        uploadItem.setNotificationId(notificationId);
                        uploadItem.setMediaList(mediaList);
                        // service call
                        serviceCall("image[]", mediaList.get(0).getUri(), uploadItem, notificationId);
                    }
                }
            }
        } else {
            showToast("Check internet connection");
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d("", "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d("", "onPermissionsDenied:" + requestCode + ":" + perms.size());
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    @AfterPermissionGranted(RC_PERM_STORAGE)
    public void getPermissions() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            galleryMultipleMediaChoose();
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_storage_camera), RC_PERM_STORAGE, perms);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //stopService(new Intent(this, FileUploaderBackgroundService.class));
    }

    private void serviceCall(String paramsName, String pathName, final UploadItem uploadItem, final int notificationId) {

        String uploadFilesStr = null;
        UploadFiles uploadFiles = null;

        // get upload media files string to shared preference
        uploadFilesStr = SharedPrefsUtils.getString(ACCOUNT_PREFS, UPLOAD_FILES);
        if (uploadFilesStr != null) {
            // convert  string  to upload item object file
            uploadFiles = getJsonFromString(uploadFilesStr, UploadFiles.class);
        }

        if (uploadFiles == null) {
            // set uploaded item count
            uploadItem.setUploadItemCount(uploadItem.getMediaList().size());

            uploadFiles = new UploadFiles();
            //  add uploaded item
            ArrayList<UploadItem> uploadItemList = new ArrayList<>();
            uploadItemList.add(uploadItem);
            uploadFiles.setUploadItemList(uploadItemList);
            // convert  upload item object file to string
            uploadFilesStr = getStringFromJson(uploadFiles);
            // save upload media files string to shared preference
            SharedPrefsUtils.set(ACCOUNT_PREFS, UPLOAD_FILES, uploadFilesStr);
        } else {
            // set uploaded item count
            uploadItem.setUploadItemCount(uploadItem.getMediaList().size());

            ArrayList<UploadItem> uploadItemList = uploadFiles.getUploadItemList();
            uploadItemList.add(uploadItem);
            uploadFiles.setUploadItemList(uploadItemList);
            // convert  upload item object file to string
            uploadFilesStr = getStringFromJson(uploadFiles);
            // save upload media files string to shared preference
            SharedPrefsUtils.set(ACCOUNT_PREFS, UPLOAD_FILES, uploadFilesStr);
        }

        // start service
        Intent intentService = new Intent(this, FileUploaderBackgroundService.class);
        intentService.putExtra("notificationId", notificationId);
        startService(intentService);
        FileUploadService.serviceCall(BASE_URL,paramsName, pathName, uploadItem, notificationId);
    }
}