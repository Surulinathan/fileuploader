package com.tartlabs.mediafileuploader.backgroudService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.tartlabs.mediafileuploader.R;
import com.tartlabs.mediafileuploader.config.UploadNotificationConfig;
import com.tartlabs.mediafileuploader.config.UploadNotificationStatusConfig;
import com.tartlabs.mediafileuploader.eventBus.AppEventBus;
import com.tartlabs.mediafileuploader.eventBus.OnCompletedEventReceiver;
import com.tartlabs.mediafileuploader.eventBus.OnFailureEventReceiver;
import com.tartlabs.mediafileuploader.eventBus.OnRetryEventReceiver;
import com.tartlabs.mediafileuploader.model.Media;
import com.tartlabs.mediafileuploader.model.UploadFiles;
import com.tartlabs.mediafileuploader.model.UploadItem;
import com.tartlabs.mediafileuploader.service.FileUploadService;
import com.tartlabs.mediafileuploader.util.SharedPrefsUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import okhttp3.MultipartBody;

import static com.tartlabs.mediafileuploader.constant.Constants.ACCOUNT_PREFS;
import static com.tartlabs.mediafileuploader.constant.Constants.BASE_URL;
import static com.tartlabs.mediafileuploader.constant.Constants.UPLOAD_FILES;
import static com.tartlabs.mediafileuploader.util.AppUtils.prepareFilePart;
import static com.tartlabs.mediafileuploader.util.ConversionUtils.getJsonFromString;
import static com.tartlabs.mediafileuploader.util.ConversionUtils.getStringFromJson;
import static com.tartlabs.mediafileuploader.util.FileUploadNotificationUtils.getNotificationConfig;
import static com.tartlabs.mediafileuploader.util.FileUploadNotificationUtils.setStartTime;

public class FileUploaderBackgroundService extends Service {
    public static final String NOTIFICATION_CHANNEL_ID = "file_uploader_1002";
    public static final String NOTIFICATION_CHANNEL_NAME = "file_uploader";
    public int notificationId = 0;

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;

    private UploadNotificationConfig uploadNotificationConfig;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if (intent != null && intent.getExtras() != null) {
            uploadNotificationConfig = getNotificationConfig(getBaseContext(), R.string.media_upload);

            AppEventBus.getInstance().asObservable().observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Object>() {
                        @Override
                        public void accept(Object event) throws Exception {
                            onEvent(event);
                        }
                    });

            // set upload start time
            setStartTime(new Date().getTime());

            notificationId = intent.getExtras().getInt("notificationId");
            // create notification
            createNotification(this);
        }


        return super.onStartCommand(intent, flags, startId);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(Object event) {
        if (event instanceof OnCompletedEventReceiver) {

            UploadNotificationStatusConfig statusConfig = uploadNotificationConfig.getCompleted();
            int notificationId = ((OnCompletedEventReceiver) event).getNotificationId();
            Object object = ((OnCompletedEventReceiver) event).getObject();
            UploadItem uploadItem = (UploadItem) object;
            ArrayList<Media> mediaList = uploadItem.getMediaList();

            if (mediaList.size() == 1) {
                // remove uploaded item
                uploadItem.getMediaList().remove(0);

                AppEventBus.getInstance().post(new OnCompletedEventReceiver(uploadItem, notificationId));
                mBuilder.setOngoing(false);
                mBuilder.setProgress(100, 100, false);
                // mBuilder.setContentText("Uploaded ( " + uploadItem.getUploadItemCount() + " ) completed in " + getElapsedTimeString());
                mBuilder.setSmallIcon(statusConfig.iconResourceID);
                mBuilder.setColor(statusConfig.iconColorResourceID);
                mBuilder.setContentText(statusConfig.message);
                mNotificationManager.notify(notificationId, mBuilder.build());
                // remove uploaded file
                removeUploadedFile(notificationId);
            } else if (mediaList.size() > 1) {
                // remove uploaded item
                uploadItem.getMediaList().remove(0);

                AppEventBus.getInstance().post(new OnCompletedEventReceiver(uploadItem, notificationId));
                //mBuilder.setContentText("Uploaded " + (uploadItem.getUploadItemCount() - mediaList.size()) + " of " + uploadItem.getUploadItemCount());
                mBuilder.setSmallIcon(statusConfig.iconResourceID);
                mBuilder.setColor(statusConfig.iconColorResourceID);
                mBuilder.setContentText(statusConfig.message);
                mNotificationManager.notify(notificationId, mBuilder.build());
                int index = 0;
                if (!mediaList.isEmpty()) {
                    for (Media media : mediaList) {
                        //very important image[]
                        Uri fileUri = Uri.parse(media.getUri());
                        MultipartBody.Part file = prepareFilePart("images[]", fileUri);
                        if (index == 0)
                            FileUploadService.serviceCall(BASE_URL,"images[]", media.getUri(), uploadItem, notificationId);
                        index += 1;
                    }
                }
                // remove uploaded file
                removeUploadedFile(notificationId);
            }
        } else if (event instanceof OnFailureEventReceiver) {
            UploadNotificationStatusConfig statusConfig = uploadNotificationConfig.getError();
            notificationId = ((OnFailureEventReceiver) event).getNotificationId();
            Object object = ((OnFailureEventReceiver) event).getObject();
            UploadItem uploadItem = (UploadItem) object;
            ArrayList<Media> mediaList = uploadItem.getMediaList();
            if (mediaList.size() > 0) {
               /* mBuilder.setSmallIcon(statusConfig.iconResourceID);
                mBuilder.setColor(statusConfig.iconColorResourceID);
                mBuilder.setContentTitle(statusConfig.title)
                        .setOngoing(true)
                        .setContentText(statusConfig.message)*/
                mBuilder.setSmallIcon(statusConfig.iconResourceID);
                mBuilder.setColor(statusConfig.iconColorResourceID);
                mBuilder.setOngoing(false);
                mBuilder.setProgress(0, 0, false);
                mBuilder.setContentText("Uploaded " + (uploadItem.getUploadItemCount() - mediaList.size()) + " of " + uploadItem.getUploadItemCount() + " Failed " + mediaList.size());
                mNotificationManager.notify(notificationId, mBuilder.build());
            }
        } else if (event instanceof OnRetryEventReceiver) {
            notificationId = ((OnRetryEventReceiver) event).getNotificationId();
            Object object = ((OnRetryEventReceiver) event).getObject();
        }
    }

    public void createNotification(Context context) {
        UploadNotificationStatusConfig statusConfig = uploadNotificationConfig.getProgress();

        Intent resultIntent = new Intent();
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(statusConfig.iconResourceID);
        mBuilder.setColor(statusConfig.iconColorResourceID);
        mBuilder.setContentTitle(statusConfig.title)
                .setOngoing(true)
                .setContentText(statusConfig.message)
                .setContentIntent(resultPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        mBuilder.setSound(null);
        mBuilder.setVibrate(new long[]{0L});
        mBuilder.build().flags |= Notification.FLAG_ONGOING_EVENT;
        mBuilder.setWhen(System.currentTimeMillis());
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance);
            notificationChannel.setDescription("no sound");
            notificationChannel.setSound(null, null);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        mBuilder.setProgress(100, 0, true);
        mNotificationManager.notify(notificationId, mBuilder.build());
    }


    private void removeUploadedFile(int notificationId) {
        String uploadFilesStr = null;
        UploadFiles uploadFiles = null;

        // get upload media files string to shared preference
        uploadFilesStr = SharedPrefsUtils.getString(ACCOUNT_PREFS, UPLOAD_FILES);

        if (uploadFilesStr != null && !uploadFilesStr.isEmpty()) {

            // convert  string  to upload item object file
            uploadFiles = getJsonFromString(uploadFilesStr, UploadFiles.class);
            ArrayList<UploadItem> uploadItemList = new ArrayList<>();

            for (UploadItem uploadItem : uploadFiles.getUploadItemList()) {
                if (uploadItem.getNotificationId() == notificationId && uploadItem.getMediaList().size() > 0) {
                    uploadItem.getMediaList().remove(0);
                }
                uploadItemList.add(uploadItem);
            }

            uploadFiles.setUploadItemList(uploadItemList);
            // convert  upload item object file to string
            uploadFilesStr = getStringFromJson(uploadFiles);
            // save upload media files string to shared preference
            SharedPrefsUtils.set(ACCOUNT_PREFS, UPLOAD_FILES, uploadFilesStr);
        }
    }
}