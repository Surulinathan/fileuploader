package com.tartlabs.mediafileuploader.eventBus;

public class OnFailureEventReceiver<T> {
    private T object;
    private int notificationId;

    public OnFailureEventReceiver(T object, int notificationId) {
        this.object = object;
        this.notificationId = notificationId;
    }

    public T getObject() {
        return object;
    }

    public int getNotificationId() {
        return notificationId;
    }
}
