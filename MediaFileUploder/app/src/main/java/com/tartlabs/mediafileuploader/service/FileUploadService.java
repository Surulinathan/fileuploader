package com.tartlabs.mediafileuploader.service;

import android.net.Uri;

import com.tartlabs.mediafileuploader.eventBus.AppEventBus;
import com.tartlabs.mediafileuploader.eventBus.OnCompletedEventReceiver;
import com.tartlabs.mediafileuploader.eventBus.OnFailureEventReceiver;
import com.tartlabs.mediafileuploader.model.UploadItem;
import com.tartlabs.mediafileuploader.network.FileUploadApi;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tartlabs.mediafileuploader.util.AppUtils.prepareFilePart;

public class FileUploadService {

    public static void serviceCall(String baseUploadUrl, String paramsName, String path, final UploadItem uploadItem, final int notificationId) {

        List<MultipartBody.Part> list = new ArrayList<>();
        MultipartBody.Part file = null;

        //very important image[]
        Uri fileUri = Uri.parse(path);
        file = prepareFilePart(paramsName, fileUri);

        Retrofit builder = new Retrofit.Builder().baseUrl(baseUploadUrl).addConverterFactory(GsonConverterFactory.create()).build();

        FileUploadApi fileUploadApi = builder.create(FileUploadApi.class);
        Call<ResponseBody> call = fileUploadApi.uploadMediaFiles(file);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppEventBus.getInstance().post(new OnCompletedEventReceiver(uploadItem, notificationId));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppEventBus.getInstance().post(new OnFailureEventReceiver(uploadItem, notificationId));
            }
        });
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }
}
