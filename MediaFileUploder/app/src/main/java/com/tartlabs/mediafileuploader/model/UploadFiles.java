package com.tartlabs.mediafileuploader.model;

import java.util.ArrayList;

public class UploadFiles {

    private ArrayList<UploadItem> uploadItemList;

    public ArrayList<UploadItem> getUploadItemList() {
        return uploadItemList;
    }

    public void setUploadItemList(ArrayList<UploadItem> uploadItemList) {
        this.uploadItemList = uploadItemList;
    }
}