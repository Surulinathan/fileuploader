package com.tartlabs.library.model;

import java.util.ArrayList;

public class UploadItem {

    private int notificationId;
    private int uploadItemCount;
    private ArrayList<Media> mediaList;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getUploadItemCount() {
        return uploadItemCount;
    }

    public void setUploadItemCount(int uploadItemCount) {
        this.uploadItemCount = uploadItemCount;
    }

    public ArrayList<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<Media> mediaList) {
        this.mediaList = mediaList;
    }
}
