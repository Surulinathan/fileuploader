package com.tartlabs.library.network;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileUploadApi {

    @Multipart
    @POST("image/upload")
    Call<ResponseBody> uploadMediaFiles(@Part MultipartBody.Part file);
}
