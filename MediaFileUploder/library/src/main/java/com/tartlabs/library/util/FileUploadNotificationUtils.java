package com.tartlabs.library.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;

import com.tartlabs.library.MainActivity;
import com.tartlabs.library.R;
import com.tartlabs.library.config.UploadNotificationConfig;

import java.util.Date;

public class FileUploadNotificationUtils {
    public static final String NOTIFICATION_CHANNEL_ID = "file_uploader_1002";
    public static final String NOTIFICATION_CHANNEL_NAME = "file_uploader";
    public static int notificationId = 0;

    private static NotificationManager mNotificationManager;
    private static NotificationCompat.Builder mBuilder;

    static long startTime;


    /**
     * Gets the elapsed time as a string, expressed in seconds if the value is {@code < 60},
     * or expressed in minutes:seconds if the value is {@code >=} 60.
     *
     * @return string representation of the elapsed time
     */
    public static String getElapsedTimeString() {
        int elapsedSeconds = (int) (getElapsedTime() / 1000);

        if (elapsedSeconds == 0)
            return "0s";

        int minutes = elapsedSeconds / 60;
        elapsedSeconds -= (60 * minutes);

        if (minutes == 0) {
            return elapsedSeconds + "s";
        }

        return minutes + "m " + elapsedSeconds + "s";
    }

    /**
     * Gets upload task's start timestamp in milliseconds.
     *
     * @return long value
     */
    public static long getStartTime() {
        return startTime;
    }

    /**
     * Sets upload task's start timestamp in milliseconds.
     *
     * @return long value
     */
    public static void setStartTime(long startTime_) {
        startTime = startTime_;
    }

    /**
     * Gets upload task's elapsed time in milliseconds.
     *
     * @return long value
     */
    public static long getElapsedTime() {
        long currentTime = new Date().getTime();
        return (currentTime - getStartTime());
    }

    public static UploadNotificationConfig getNotificationConfig(Context context,@StringRes int title) {
        UploadNotificationConfig config = new UploadNotificationConfig();

        PendingIntent clickIntent = PendingIntent.getActivity(
                context, 1, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        config.setTitleForAllStatuses(context.getString(title))
                .setRingToneEnabled(true)
                .setClickIntentForAllStatuses(clickIntent)
                .setClearOnActionForAllStatuses(true);

        config.getProgress().message = context.getString(R.string.uploading);
        config.getProgress().iconResourceID = R.drawable.ic_stat_notification;
        config.getProgress().iconColorResourceID = Color.BLUE;

        config.getCompleted().message = context.getString(R.string.upload_success);
        config.getCompleted().iconResourceID = R.drawable.ic_stat_notification;
        config.getCompleted().iconColorResourceID = Color.GREEN;

        config.getError().message = context.getString(R.string.upload_error);
        config.getError().iconResourceID = R.drawable.ic_stat_notification;
        config.getError().iconColorResourceID = Color.RED;

        config.getCancelled().message = context.getString(R.string.upload_cancelled);
        config.getCancelled().iconResourceID = R.drawable.ic_stat_notification;
        config.getCancelled().iconColorResourceID = Color.YELLOW;

        return config;
    }
}
